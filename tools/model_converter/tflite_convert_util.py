#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from genericpath import exists
import os
import sys
import argparse
import wget
import zipfile
import tensorflow as tf
from tensorflow.keras.models import load_model

sys.path.append(os.path.join(os.path.dirname(
    os.path.realpath(__file__)), '..', '..'))
from common.utils import get_custom_objects

#                               Technique	    Benefits	        Hardware
# Dynamic range quantization	4x smaller,     2x-3x speedup	    CPU
# Full integer quantization	    4x smaller,     3x+ speedup	CPU,    Edge TPU, Microcontrollers
# Float16 quantization	        2x smaller,     GPU acceleration	CPU, GPU

# Reference : https://www.tensorflow.org/lite/performance/post_training_quantization

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..')

def fp32_quantization(keras_model_file, output_file):
    custom_object_dict = get_custom_objects()
    model = load_model(keras_model_file, custom_objects=custom_object_dict)
    converter = tf.lite.TFLiteConverter.from_keras_model(model)

    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model = converter.convert()
    with open(output_file, "wb") as f:
        f.write(tflite_model)


def fp16_quantization(keras_model_file, output_file):
    custom_object_dict = get_custom_objects()
    model = load_model(keras_model_file, custom_objects=custom_object_dict)
    converter = tf.lite.TFLiteConverter.from_keras_model(model)

    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.target_spec.supported_types = [tf.float16]

    tflite_model = converter.convert()
    with open(output_file, "wb") as f:
        f.write(tflite_model)


def int8_quantization(keras_model_file, input_shape, output_file):
    custom_object_dict = get_custom_objects()
    model = load_model(keras_model_file, custom_objects=custom_object_dict)

    converter = tf.lite.TFLiteConverter.from_keras_model(model)

    # Calibrate dataset : https://github.com/ultralytics/yolov5/releases/download/v1.0/coco128.zip

    coco_dir = os.path.join(ROOT_DIR, 'example/coco128')
    example_dir = os.path.join(ROOT_DIR, 'example')

    if not os.path.exists(coco_dir):
        url = 'https://github.com/ultralytics/yolov5/releases/download/v1.0/coco128.zip'
        wget.download(url)
        with zipfile.ZipFile('coco128.zip', 'r') as zip_ref:
            zip_ref.extractall(example_dir)
        os.remove('coco128.zip')

    def representative_dataset():
        images_path = os.path.join(ROOT_DIR, 'example/coco128/images/train2017')
        images_pattern = images_path + '/*.jpg'

        rep_ds = tf.data.Dataset.list_files(images_pattern)
        for image_path in rep_ds:
            img = tf.io.read_file(image_path)
            img = tf.io.decode_image(img, channels=3)
            img = tf.image.convert_image_dtype(img, tf.float32)
            resized_img = tf.image.resize(img, (input_shape, input_shape))
            resized_img = resized_img[tf.newaxis, :]
            yield [resized_img]

    converter.optimizations = [tf.lite.Optimize.DEFAULT]

    converter.representative_dataset = tf.lite.RepresentativeDataset(
        representative_dataset)
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    converter.inference_input_type = tf.uint8
    converter.inference_output_type = tf.uint8

    tflite_model = converter.convert()
    with open(output_file, "wb") as f:
        f.write(tflite_model)


def main():
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS,
                                     description='TF 2.x post training integer quantization converter')

    parser.add_argument('-k', '--keras_model_file', required=True,
                        type=str, help='path to keras model file')
    parser.add_argument('-i', '--input_shape', type=int,
                        help='model image input shape, default=%(default)s', default='416')
    parser.add_argument('-q', '--quant_type', required=True,
                        type=str, help='Quantization type : fp32/fp16/int8')

    args = parser.parse_args()

    output_name = os.path.basename(args.keras_model_file.split(".")[0])
    output_name = output_name + '_' + args.quant_type + ".tflite"
    output_file = os.path.join(ROOT_DIR, 'weights', output_name)

    print("Saving TFLite file : {}".format(output_file))

    if args.quant_type == 'fp32':
        fp32_quantization(args.keras_model_file, output_file)
    elif args.quant_type == 'fp16':
        fp16_quantization(args.keras_model_file, output_file)
    elif args.quant_type == 'int8':
        int8_quantization(args.keras_model_file,
                          args.input_shape, output_file)
    else:
        print("Do not support this quantization type : {}".format(args.quant_type))


if __name__ == '__main__':
    main()
